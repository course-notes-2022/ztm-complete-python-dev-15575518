# Developer Fundamentals: 1

1. **Don't read the dictionary**. Do not try to learn every single function,
   method, tip/trick etc. in the programming language you're learning. Get
   comfortable **Googling things**, and learn **what's useful to you at the
   given time**.

Some things are used very often, while others are rarely used at all. Focus on
the 20% that gets used 80% of the time, and know how to find what you need when
you need it.
