# Tuples 2

```py
my_tuple = (1,2,3,4,5)

new_tuple = my_tuple[1:2]

x = my_tuple[0] # 1

a,b,c, *other = (1, 2, 3, 4, 5)

print(my_tuple.count(5)) # 1

print(my_tuple.index(5)) # 4

print(len(my_tuple)) # 6
```
