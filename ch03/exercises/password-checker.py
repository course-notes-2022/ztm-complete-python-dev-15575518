username = input('Please input your username: ')
password = input('Please input your password: ')
pwd_len = len(password)
redacted = ""

for char in password:
    redacted+= '*'

print(f"Hello {username}, your password {redacted} is {pwd_len} characters long")