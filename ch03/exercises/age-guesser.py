import datetime

birth_year = int(input('What year were you born?'))

current_year = datetime.datetime.now().year

print(f"Your age is {current_year - birth_year}")