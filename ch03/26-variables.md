# Variables

**Variables** store information that can be used in our programs. In Python, we
can simply **name** our variables to declare them. Once we assign a value to our
variables, we can use them in our programs:

```py

iq = 190 # initialize the variable 'iq' with the value 190

print(iq) # 190
```

## Best Practices for Defining Variables

- `snake_case`
- Start with lowercase letters or underscore (underscore indicates a private
  variable)
- Can contain letters, numbers, or underscores
- Case-sensitive
- Cannot use reserved keywords

Variables can be re-assigned after they are initialized. **Constants** cannot.
Python does not have constants, but a convention is to create variables that are
**meant** to be constants with all-caps name. Variables that begin with `__`
also should not be changed.

You can initialize multiple variables at a time by separating them with
**commas**:

```py
a,b,c = 1,2,3
```
