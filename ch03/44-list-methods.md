# List Methods

```py
basket = [1, 2, 3, 4, 5]

print(len(basket)) # 5; len() returns the length of the list

# Add to the end of the list
new_list = basket.append('foo') # note that append updates the list IN PLACE and does not output a new list; new_list returns 'None'

# Insert an item into a list
basket.insert(6, 100) # insert 100 at index 6 of basket

# Extend a list
basket.extend([102, 103]) # takes an iterable and appends the contents

# Remove
basket.pop() # "pops" the last item from the end of the list

basket.pop(0) # removes item at 0 index and returns removed item

basket.remove(102) # removes the passed value from the list

basket.clear() # removes all items from the list
```
