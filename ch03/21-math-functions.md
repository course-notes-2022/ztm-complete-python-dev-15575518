# Math Functions

Python provides different functions for us to perform mathematical calculations
**built in**:

```py

print(round(3.1)) # 3

print(abs(-7)) # 7; abs returns the absolute value
```
