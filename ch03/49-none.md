# None

**None** is a special Python data type that refers to the **absence of value**.
It is similar to `null` in other programming languages.
