# Type Conversion

You can convert data types in Python using builtin functions like `str`:

```py
print(type(str(100))) # <class 'str'>
```
