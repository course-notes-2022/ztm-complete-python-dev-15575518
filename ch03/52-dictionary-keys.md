# Dictionary Keys

Dictionary keys must be **immutable**: numbers, booleans, strings. Lists are
**mutable**, and therefore cannot be valid dictionary keys. Most of the time,
keys will usually be something descriptive like a string.

Keys must also be **unique**. In the case of duplicate keys, the last will
overwrite any previous duplicates.
