# Lists

**Lists** are **ordered sequences of objects** that can be of any type:

```py
mylist = [1, 2, 3, 4, 5]

mylist_2 = [1, 2, 'a', True]
```

Lists are extremely important. They correspond to **arrays** in other
programming languages.

## Data Structures

Lists are our first **data structure**. Data structures are **organizations of
objects**, that have different pros and cons of **accessing and removing data**.
Lists organize items inside them in **sequential memory locations**.
