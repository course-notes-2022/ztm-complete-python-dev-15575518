# Immutability

Strings in Python are **immutable**, meaning you cannot re-assign **part** of
the string once its initialized. You **can** assign a **new value** to the
string afterwards, however.
