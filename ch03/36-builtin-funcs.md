# Builtin Functions

- `len()`: returns the length of a string

## Builtin Methods

**Methods** are similar to functions, but they are **owned** by something (e.g.
a class, such as `str`).

```py
quote = 'to be or not to be'

# String builtin methods

print(quote.upper()) # 'TO BE OR NOT TO BE'

print(quote.capitalize()) # 'To be or not to be'

print(quote.find('be')) # 3; index where substring begins

print(quote.replace('be', 'me')) # 'to me or not to me'

```
