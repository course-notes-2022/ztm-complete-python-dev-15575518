# Strings

**Strings** are sequences of characters/pieces of text, enclosed either in `''` or `""`.

```py

username = "supercoder"
password = "supersecret"

long_string = '''
WOW

0 0

---
''' # Enclose multiple lines in three single quotes

```
