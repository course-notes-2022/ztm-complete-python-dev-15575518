# Escape Sequences

**Escape sequences** allow you to include special characters inside strings:

```py
weather = "it's sunny"

weather_2 = 'it\'s sunny' # Escape the intermediate single quote with the backslash character

```

Escape sequences signify to Python that whatever follows immediately after the
backslash should be interpreted as a string.

You can also include escape sequences to add tabs (`\t`), newlines (`\n`), etc.
