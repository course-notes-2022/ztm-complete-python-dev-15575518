# String Concatenation

String concatenation means adding strings together:

```py
print('hello' + ' world') # "hello world"
```
