# Booleans

**Booleans** (`bool` in Python) can be either `True` or `False`.

```py

name = 'Andrei'
is_cool = False

is_cool = True

print(bool(1)) # True

print(bool(0)) # False
```

Booleans are logical values that we can use to **control the flow** of our
programs.
