# Numbers in Python

We have the **int** and **float** data types. **int** stands for integers (whole
numbers), and **float** stands for floating point numbers:

```py

# Fundamental data types

print(2 + 4) # print the sum of two integers
print(2 - 4)
print(2 * 4)
print(2 / 4)
```

A float **takes up more space in memory than an integer**. The issue is that
when you have decimal places, it's hard to represent in binary, so a float is
actually stored in **two different locations**: one for the integer portion, and
another for the decimal portion.

Python automatically converts numbers to the type that makes the most sense.

```py
print(2**3) # 8; exponentiation

print (5//4) # 1; returns an integer, rounded down

print(5 % 4) # 1; returns the remainder
```
