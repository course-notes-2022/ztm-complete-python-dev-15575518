# Dictionary Methods 2

```py

user = {
    'basket': [1, 2, 3],
    'greet': 'hello',
    'age': 20,
}

print('basket' in user) # True

print('size' in user.keys()) # False; keys() checks keys of dictionary

print('hello' in user.values()) # True; values() checks values of dictionary

print(user.items())

user2 = user.copy() # Creates a new dictionary with the values of the first dictionary copied


user.pop('age') # 'pops' the value off of the dictionary and returns it

print(user.update({'age': 55})) # upserts key/value pair to dictionary

user.clear() # Clears attributes from dictionary


```
