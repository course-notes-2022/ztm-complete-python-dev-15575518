# Dictionaries

**Dictionaries** (`dict`) is a Python data type and data structure. It is
similar to a `Map` or `object` in other programming languages. They consist of
**unorderd key-value pairs**:

```py

dictionary = { # curly braces denote a dictionary
    'a': [1, 2, 3],
    'b': 2,
    'x': 'hello'
    'y': True # dictionaries attributes can be any data type
}

print(dictionary['b']) # 2
```
