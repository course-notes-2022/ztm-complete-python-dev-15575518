# String Indexes

Strings are stored as an **ordered sequence of characters** in memory. One of
the most useful things we can do with strings is to access **part of the string
by its index**:

```py

selfish = 'me me me'

print(selfish[0]) # m; "m" is the character at index 0 of selfish
```

When we use square brackets in Python, we can use them in the following way:

```py
# [start:stop]

print(selfish[0:5]) # " me me "

# [start:stop:stepover]
print(selfish[0:6:2]) # prints every second character, starting at index 0 and ending at index 6

# You can also omit the start and stop:
print(selfish[:6]) # prints starting at 0 index up to 6

print(selfish[3:]) # prints starting a index 3 to end of string

print([::1]) # print every character

print([-1]) # negative values start from the END of string

print(selfish[::-1]) # 76543210; reverses the string
```
