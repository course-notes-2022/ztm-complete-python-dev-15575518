# Developer Fundamentals 3

## Understanding Data Structures

A good programmer must understand the **trade-offs** between data structures,
and **when to use which structure**.

A **list** is sorted. **Dictionaries** are unsorted. When _order matters_, a
list is more appropriate.

Dictionaries have **keys**, which give additional information about the data
held in the structure.
