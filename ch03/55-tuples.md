# Tuples

**Tuples** are like **immutable lists**:

```py

my_tuple = (1, 2, 3, 4, 5)

my_tuple[1] = 'z' # NO; this will throw an error

```

Why use tuples?

- Makes code safer in the case you want your list to not change
- Slightly faster than lists
- Tuples are valid dictionary keys
