# Operator Precedence

Remember `PEMDAS` from your math classes:

- Parentheses `()`
- Exponents `**`
- Multiplication `*`
- Division `/`
- Addition `+`
- Subtraction `-`

This works the same in all programming languages.

## Exercise: Operator Precedence

```py
# Guess the output of each answer before you click RUN
# Try to write down your answer before and see how you do... keep it mind I made it a little tricky for you :)

print((5 + 4) * 10 / 2) # (9) * (10/2) = 9 * 5 = 45

print(((5 + 4) * 10) / 2) # ((9) * 20) / 2 = 90 / 2 = 45

print((5 + 4) * (10 / 2)) # ((9) * (5)) = 45

print(5 + (4 * 10) / 2) # 5 + (40) / 2 = 5 + 20 = 25

print(5 + 4 * 10 // 2) # 5 + (4 * 10) // 2 = 5 + (40) // 2 = 45 // 2 = 23
```
