# Developer Fundamentals: 2

## Code Comments: Key Guidelines

- Code comments should **add valuable context** to the code. Ideally, code
  should be easy-to-read and self-explanatory.
