# List Slicing

We can **slice** lists similarly to how we slice strings:

```py

mylist = [
    'notebooks',
    'sunglasses',
    'toys'
    'grapes'
    ]

print(mylist[0:2]) # ['notebooks', 'sunglasses']

print(mylist[0::2]) # ['notebooks', 'toys']

mylist[0] = 'calculator'

print(mylist[0]) # calculator; Lists ARE mutable

print(mylist[0:3]) # ['calculator', 'sunglasses', 'toys']

print(mylist) # ['calculator', 'sunglasses', 'toys', 'grapes']
```

Note that list slicing creates a **copy** of the list. Lists are created **by
reference**. To copy a list:

```py
new_cart = mylist[:] # Creates a new, separate copy of mylist
```
