# Augmented Assignment Operator

Use **augmented assingment operators** to update the value of variables in a
more concise way:

```py

some_value = 5

some_value += 2 # augmented assignment operator
```
