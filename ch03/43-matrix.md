# Matrix

A **matrix** is a way to describe multi-dimensional lists:

```py
matrix = [
    [1, 2, 3],
    [2, 4, 6],
    [7, 8, 9]
]

```

It's a **list of lists**. Matrices come up frequently in **machine learning**
topics.

## Accessing Items in Matrices

```py

print(matrix[0][1]) # 2
```
