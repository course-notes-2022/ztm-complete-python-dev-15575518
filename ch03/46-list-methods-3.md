# List Methods 3

```py

basket = ['a', 'b', 'c', 'd', 'e', 'd']

basket.sort() # ['a', 'b', 'c', 'd', 'd', 'e']; sorts basket in place and returns 'None'

print(sorted(basket)) # produces a new, sorted array and returns it

new_basket = basket.copy() # copies list and returns a new list

basket.reverse() # reverses indexed items of array in place
```
