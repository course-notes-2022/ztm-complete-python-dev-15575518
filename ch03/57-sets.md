# Sets

**Sets** are unordered collections of **unique** objects:

```py

my_set = {1,2,3,4,5} # No duplicates allowed

my_set.add(100)

my_set.add(2) # NO; 2 will not be added again

# Create a new set from a list

my_list = [1,2,3,4,5,5]

print(set(my_list)) # {1,2,3,4,5}
```

You cannot access items in a set by referring to an **index** or **key**. You
**can** loop through the set items using a `for` loop, or ask if a specified
value is present in a set by using the `in` keyword:

```py

thisset = {"apple", "banana", "cherry"}

for x in thisset:
    print(x)

print("banana" in thisset)
```

```py

my_set = {1,2,3,4,5}

new_set = my_set.copy() # Create a copy of a set
```
