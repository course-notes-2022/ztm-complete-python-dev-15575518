# Optional: `bin()` and `complex`

`complex` is an additional data type. It is a third data type for numbers, that
you may (never) use for complex mathematical calculation.

Ints and floats get stored as binary. `bin` returns the binary version of an int
or float:

```py

print(bin(5)) # 0b101

print(int('0b101', 2)) # 5
```
