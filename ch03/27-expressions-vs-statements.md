# Expressions vs. Statements

An **expression** is a piece of code that **produces a value**.

A **statement** is an entire line of code that performs some action.

| Expression | Statement           |
| ---------- | ------------------- |
| `iq / 5`   | `user_age = iq / 5` |
