# Formatted Strings

```py

name = 'Johnny'

age = 55

print('hi' + name + '. You are ' + str(age) + ' years old') # kind of cumbersome

print(f'hi {name}. You are {age} years old') # better!

print('hi {}. You are {} years old'.format('Johnny', '55')) # python2, will work in python3

print('hi {}. You are {} years old'.format(name, age))

print('hi {0}. You are {1} years old'.format(name, age)) # pass variables by index
```
