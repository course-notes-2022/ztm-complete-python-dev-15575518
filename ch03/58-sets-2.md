# Sets 2

## Set methods

```py

my_set = {1,2,3,4,5}
your_set = {4,5,6,7,8,9,10}

# difference() returns the difference between sets
print(my_set.difference(your_set)) # {1,2,3}

# discard() removes an element from a set if it is a member
my_set.discard(5)

print(my_set) # {1,2,3,4}

# difference_update() removes all elements of ANOTHER set from THIS set
my_set.difference_update(your_set)

print(my_set) # {1,2,3}

# intersection() returns the intersection of two sets
print(my_set.intersection(your_set)) # {4, 5}

# isdisjoint() returns a boolean value indicating whether or not the sets have any common elements

print(my_set.isdisjoint(your_set)) # True

# issubset() returns a boolean value indicating whether set A is contained in set B
print(my_set.issubset(your_set)) # False

# issuperset() returns a boolean value indicating whether set A contains set B


# union() unites the sets and removes duplicates. Returns a new set
print(my_set.union(your_set)) # {1,2,3,4,5,6,7,8,9,10}
```
