# Exercise: Password Checker

Create a password checker:

- Ask the user for a username and password
- Print a message indicating the length of the password:

```py
print('Hello {user} password {******} is {6} letters long')
```

## My Solution

```py
username = input('Please input your username: ')
password = input('Please input your password: ')
pwd_len = len(password)
redacted = ""

for char in password:
    redacted+= '*'

print(f"Hello {username}, your password {redacted} is {pwd_len} characters long")
```
