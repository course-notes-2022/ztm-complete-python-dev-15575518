# Common List Patterns

```py
basket = ['a', 'x', 'b', 'c', 'd', 'e', 'd']
basket.sort()
basket.reverse()

# Reverse list using slicing; note that a NEW list is returned
print(basket[::-1]) # ['a', 'b', 'c', 'd', 'd', 'e', 'x']

# The old list is unaffected
print(basket) # ['x', 'e', 'd', 'd', 'c', 'b', 'a']

# Generate a list using the range() function
print(list(range(1, 100))) # [1, 2, 3...99]; range(start, stop)

sentence = ' '
new_sentence = sentence.join(['hi', 'my', 'name', 'is', 'slimshady'])

print(new_sentence) # 'hi my name is slimshady'
```
