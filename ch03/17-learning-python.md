# Learning Python

There are 4 key things you need to master when learning a programming language:

1. **Terms**, such as "statements", "variables", "methods" etc.

2. **Data types**, or the different ways of storing data in a program

3. **Actions** that you can perform in the language

4. **Best practices** for writing well-structured programs in the language

We'll start this chapter by learning about the **data types**.
