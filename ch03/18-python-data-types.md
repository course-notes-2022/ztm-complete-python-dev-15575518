# Data Types

Python has several **fundamental data types** available to us:

1. **int**
2. **float**
3. **bool**
4. **str**
5. **list**
6. **tuple**
7. **set**
8. **dict**

We have these data types, and we need to learn how to **manipulate** them.

We also have **custom types**, called **classes**, in Python, and **specialized
data types** that are not built in to Python, but are available from other
packages and modules in Python.

Finaly, the data type **None** exists, and indicates the **absence of value**.
