# Dictionary Methods

```py
user = {
    'basket': [1,2,3],
    'greet': 'hello'
}

print(user['age']) # Throws error; key doesn't exist

print(user.get('age')) # None; does not throw error

print(user.get('age', 55)) # 55; gets age from dictionary, or default value 55 if age doesn't exist

# Alternate syntax for creating dictionaries
user2 = dict(name='jon-jon')
```
