# Exercise: Type Conversion

Create a program that guesses the user's age. The program should:

- Ask the user for his/her birth year
- Calculate and output the user's age based on the birth year

```py
birth_year = input('what year were you born?')

```

## My Solution

```py
import datetime

# Note that you need to convert the input to an integer to do math with it
birth_year = int(input('What year were you born?'))

current_year = datetime.datetime.now().year

print(f"Your age is {current_year - birth_year}")
```
