# List Methods 2

```py

basket = [1, 2, 3, 4, 5]

print(basket.index(2)) # 1; returns the INDEX of first argument in array

print(basket.index(3, 0, 2)) # print(value, start, stop); starts looking for 'value' at 'start'

print('d' in basket) # False; check for existence of 'd' in list

basket.count('d') # 0; count number of occurrences of 'd' in list
```
